#!/bin/sh

minecraftDir=~/games/minecraft/

rm -r libs natives
mkdir libs natives

# get linux natives here
#http://store.ttyh.ru/libraries/org/lwjgl/lwjgl/lwjgl-platform/2.9.4-nightly-20150209/

find $minecraftDir -type d -name natives -exec cp -r {} . \; -quit
find $minecraftDir -type f -iname *.jar -exec cp {} libs/ \;

#./gradlew run
./gradlew clean jar

java \
  -Djava.library.path=natives \
  -jar dist/mc-launcher.jar
