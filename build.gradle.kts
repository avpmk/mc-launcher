import java.nio.file.Files

plugins {
    application
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

val distDir     = "dist"
val distLibsDir = "libs"

application.mainClassName = "ru.msk.slavapetrov.mclauncher.Launcher"

dependencies {
    compile(fileTree("dir" to "libs", "include" to "*.jar"))
}


val projectRoot = projectDir.toPath()

tasks {
    withType<Jar> {
        destinationDirectory.set(
                projectRoot.resolve(distDir).toFile()
        )

        fun libsToAddToCp() = configurations.runtime.get()
        manifest {
            attributes["Main-Class"] = application.mainClassName

            val libs = libsToAddToCp()
            if (!libs.isEmpty) {
                attributes["Class-Path"] = libs.joinToString(
                        separator = " ",
                        transform = { file -> "$distLibsDir/${file.name}" }
                )
            }
        }

        doLast {
            val distLibsDirPath = projectRoot.resolve(distDir).resolve(distLibsDir)
            delete(distLibsDirPath)

            val libs = libsToAddToCp()
            if (!libs.isEmpty) {
                Files.createDirectories(distLibsDirPath)
                for (file in libs) {
                    Files.copy(
                            file.toPath(),
                            distLibsDirPath.resolve(file.name)
                    )
                }
            }
        }
    }

    named("clean").configure {
        delete(projectRoot.resolve(distDir))
    }
}


/** for app launched with 'run' task */
application.applicationDefaultJvmArgs = listOf(
        "-Djava.library.path=${projectRoot.resolve("natives")}"
)
