package ru.msk.slavapetrov.mclauncher;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Launcher {

    public static void main(String[] args) {
        Path dirRoot = jarDir(Launcher.class);
        String
                userName  = "Slava",
                ver       = "ForgeOptiFine 1.8.9",
                assetsDir = "assets";

        net.minecraft.client.main.Main.main(
                new String[]{
                        "--username", userName,
                        "--version", ver,
                        "--accessToken", "0",
                        "--userProperties", "{}",
                        "--gameDir", dirRoot.toString(),
                        "--assetsDir", subDir(dirRoot, assetsDir),
                        "--assetIndex", ver,
                        "--userType legacy",
                        "--tweakClass net.minecraftforge.fml.common.launcher.FMLTweaker"
                }
        );
    }

    static Path jarDir(Class cls) {
        try {
            URI uriJarLocation = cls.getProtectionDomain().getCodeSource().getLocation().toURI();
            return Paths.get(uriJarLocation).getParent();
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    static String subDir(Path dir, String subElement) {
        return dir.resolve(subElement).toString();
    }
}